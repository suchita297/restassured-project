Endpoint is : 
https://reqres.in/api/users/2

 RequestBody is : 
{
    "name": "morpheus",
    "job": "zion resident"
}

ResponseHeader is : 
Date=Sun, 21 Apr 2024 12:34:45 GMT
Content-Type=application/json; charset=utf-8
Transfer-Encoding=chunked
Connection=keep-alive
Report-To={"group":"heroku-nel","max_age":3600,"endpoints":[{"url":"https://nel.heroku.com/reports?ts=1713702885&sid=c4c9725f-1ab0-44d8-820f-430df2718e11&s=JPr4TdBO%2Fs4%2BJv3H4heTvAV5O%2BsEavR7LKlpeHN7c%2Bg%3D"}]}
Reporting-Endpoints=heroku-nel=https://nel.heroku.com/reports?ts=1713702885&sid=c4c9725f-1ab0-44d8-820f-430df2718e11&s=JPr4TdBO%2Fs4%2BJv3H4heTvAV5O%2BsEavR7LKlpeHN7c%2Bg%3D
Nel={"report_to":"heroku-nel","max_age":3600,"success_fraction":0.005,"failure_fraction":0.05,"response_headers":["Via"]}
X-Powered-By=Express
Access-Control-Allow-Origin=*
Etag=W/"50-Dg/n1c42XINKcQDD1w9DaErlPKg"
Via=1.1 vegur
CF-Cache-Status=DYNAMIC
Server=cloudflare
CF-RAY=877d68f91b468930-SIN
Content-Encoding=gzip

ResponseBody is : 
{"name":"morpheus","job":"zion resident","updatedAt":"2024-04-21T12:34:45.723Z"}