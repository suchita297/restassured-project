package API_trigger_Reference;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
public class delete_API_trigger_Reference_Requestspecification_Rsponse_class {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// step 1 Declare needed variables
	    String hostname = "https://reqres.in";
		String resource = "/api/users/2";
			  
		// step 2 : trigger the API 
		
		//Step 2  build the request specification using requestspecification
		 RequestSpecification req_spec = RestAssured.given();
		
		// step 2.2 trigger the API
		 
		 Response response = req_spec.delete(hostname+resource);
		 // step 3 extract the status code
		 
		 int statuscode = response.statusCode();
		 System.out.println(statuscode);
		
		
	}

}
