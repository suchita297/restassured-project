package API_trigger_Reference;

import io.restassured.RestAssured;
import static io.restassured.RestAssured.given;

public class delete_API_trigger_refernce {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// step 1 Declare needed variables
		       String hostname = "https://reqres.in";
				String resource = "/api/users/2";
				
				// step 2 configure API and trigger
				RestAssured.baseURI=hostname;
				String responseBody= given().when().delete(resource).then().extract().response().asString();
				System.out.println(responseBody);
				
				 given().log().all().when().delete(resource).then().log().all().extract().response().asString();
					
				

	}

}
