package API_trigger_Reference;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import static io.restassured.RestAssured.given;

import org.testng.Assert;
public class get_API_trigger_Refernce {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//step 1 Declare needed variables
		String hostname = "https://reqres.in";
				String resource = "/api/users?page=2";
				// step 2 declare the expected result
				int [] id ={7,8,9,10,11,12};
				String[] first_name = {"Michael", "Lindsay", "Tobias","Byron","George","Rachel"};
				String[] last_name = { "Lawson", "Ferguson", "Funke","Fields","Edwards","Howell"};
				String[] email = { "michael.lawson@reqres.in","lindsay.ferguson@reqres.in","tobias.funke@reqres.in", "byron.fields@reqres.in", "george.edwards@reqres.in","rachel.howell@reqres.in"};
				String[] avatar = {"https://reqres.in/img/faces/7-image.jpg","https://reqres.in/img/faces/8-image.jpg","https://reqres.in/img/faces/9-image.jpg","https://reqres.in/img/faces/10-image.jpg","https://reqres.in/img/faces/11-image.jpg", "https://reqres.in/img/faces/12-image.jpg"};
				
					
				//step 2 configure the API and trigger
				RestAssured.baseURI= hostname;
				
				String responseBody = given().when().get(resource).then().extract().response().asString();
				System.out.println(responseBody);
				
				// step 3 : parse the responseBody using JsonPath extract responseBody parameter
				
				JsonPath jsp_res = new JsonPath(responseBody);
				int length = jsp_res.getInt("data.size()");
				
				System.out.println(length);
				
				// step 3.1 : parse the ResponseBody parameter using JsonPath
				
				for (int i=0;i<length;i++)
				{
					
				int 	
					
				expected_id = id[i];	
				String expected_first_name = first_name[i];
				String expected_last_name = last_name[i];
				String expected_email = email[i];
				String expected_avatar = avatar[i];
				
				int res_id = jsp_res.getInt("data["+i+"].id");
			    String res_first_name = jsp_res.getString("data["+i+"].first_name");
				String res_last_name = jsp_res.getString("data["+i+"].last_name");
				String res_email = jsp_res.getString("data["+i+"].email");
				String res_avatar = jsp_res.getString("data["+i+"].avatar");
				
				
				
				//  step 4 : validate using TestNG Assertion
				
			Assert.assertEquals(res_first_name,expected_first_name);
		    Assert.assertEquals(res_last_name,expected_last_name);
		    Assert.assertEquals(res_email,expected_email);
			Assert.assertEquals(res_id,expected_id);
			Assert.assertEquals(res_avatar,expected_avatar);
				
				

	}

}}
