package API_trigger_Reference;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;

public class patch_API_trigger_Reference {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// step 1 Declare the needed variables
		        String hostname ="https://reqres.in";
		        String resource = "/api/users/2";		
				String headername = "content-type";
				String headervalue = "application/json";
				String requestBody = "{\r\n"
						+ "    \"name\": \"morpheus\",\r\n"
						+ "    \"job\": \"zion resident\"\r\n"
						+ "}";
				// step 2 configure the APIand trigger
				RestAssured.baseURI=hostname;
				String responseBody =given().header(headername,headervalue).body(requestBody).when().patch(resource).then().extract().response().asString();
                System.out.print(responseBody);
                 
             // step 3 parse the ResponseBody using json path extract ResponseBody parameter
                JsonPath jsp_res = new JsonPath(responseBody);
                String res_name = jsp_res.getString("name");
                String res_job = jsp_res.getString("job");
                String res_updatedAt = jsp_res.getString("updatedAt");
                res_updatedAt = res_updatedAt.toString().substring(0,11);
                
                
                // step 4 parse the RequestBody using json path extract RequestBody parameter
                
                JsonPath jsp_req = new JsonPath(requestBody); 
                String req_name = jsp_req.getString("name");
                String req_job = jsp_req.getString("job");
                
                
                
                // Generate expected Date
                 LocalDateTime currentDate = LocalDateTime.now();
                String expecteddate = currentDate.toString().substring(0,11);
                
                // step 5 validate using TestNG Assertion
                 Assert.assertEquals(res_name,req_name,"Name in ResponseBody is not equal to name sent in RequestBody" );
                 Assert.assertEquals(res_job,req_job,"Job in ResposneBody is not equal to job sent in RequestBody");
                 Assert.assertEquals(res_updatedAt,expecteddate,"createdAt is in ResponseBody is not equal to createdAt in request");
		           
                
                
	}
	
	
	
	

}
