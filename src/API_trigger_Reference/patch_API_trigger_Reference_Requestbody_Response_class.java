package API_trigger_Reference;

import java.time.LocalDateTime;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class patch_API_trigger_Reference_Requestbody_Response_class {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// step 1 Declare the needed variables
        String hostname ="https://reqres.in";
        String resource = "/api/users/2";		
		String headername = "content-type";
		String headervalue = "application/json";
		String requestBody = "{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"zion resident\"\r\n"
				+ "}";
		// Step 2 : build the request specification using Requestspecification 
					RequestSpecification req_spec = RestAssured.given();
					
					// step 2.1 set the header
								req_spec.header(headername,headervalue);
								
								// step 2.2 set the requestBody
								req_spec.body(requestBody);
								
								
								//step 2.3 trigger the API 
								   Response response = req_spec.patch(hostname + resource);
								  System.out.println(response);
								   
								//  2.4 extract the status code 
								    int statuscode = response.statusCode();
								    System.out.println(statuscode);
								   // 3 :parse the responseBody parameter
								    
								    ResponseBody responseBody = response.getBody();
								     System.out.println(responseBody.asString());
								      
								  String res_name = responseBody.jsonPath().getString("name");
						          String res_job = responseBody.jsonPath().getString("job");
						          
						          String res_updatedAt = responseBody.jsonPath().getString("updatedAt");
						          res_updatedAt = res_updatedAt.toString().substring(0, 11);
						          
						          
						          // step 4 parse the RequestBody using json path extract RequestBody parameter
					                
					                JsonPath jsp_req = new JsonPath(requestBody); 
					                String req_name = jsp_req.getString("name");
					                String req_job = jsp_req.getString("job");
					                
					                
					                
					                //4.1:  Generate expected Date
					                 LocalDateTime currentDate = LocalDateTime.now();
					                String expecteddate = currentDate.toString().substring(0,11);
					                
					                // step 5 validate using TestNG Assertion
					                 Assert.assertEquals(res_name,req_name,"Name in ResponseBody is not equal to name sent in RequestBody" );
					                 Assert.assertEquals(res_job,req_job,"Job in ResposneBody is not equal to job sent in RequestBody");
					                 Assert.assertEquals(res_updatedAt,expecteddate,"createdAt is in ResponseBody is not equal to createdAt in request");
							           
					                
						          
	}

}
