package API_trigger_Reference;
import java.time.LocalDateTime;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.specification.RequestSpecification;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class post_API_Refernce_Requestspecificatio_Response_class {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//step1 : Declare the needed variables
        String hostname = "https://reqres.in";
		String resource = "/api/users";
		String headername= "content-type";
		String headervalue = "application/json";
		String requestBody = "{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"leader\"\r\n"
				+ "}";
		// Step 2 : build the request specification using Requestspecification 
		RequestSpecification req_spec = RestAssured.given();
		
		// step 2.1 set the header
		req_spec.header(headername,headervalue);
		
		// step 2.2 set the requestBody
		req_spec.body(requestBody);
		
		
		//step 2.3 trigger the API 
		   Response response = req_spec.post(hostname + resource);
		   System.out.println(response);
		   
		//  2.4 extract the status code 
		    int statuscode = response.statusCode();
		    System.out.println(statuscode);
		   


		   //  2.5 fetch the response body parameter 
		    
		     ResponseBody responseBody = response.getBody();
		     System.out.println(responseBody.asString());
		     
		 
		    String res_name = responseBody.jsonPath().getString("name");
	         String res_job =  responseBody.jsonPath().getString("job");
	         String res_id =  responseBody.jsonPath().getString("id");
	         String res_createdAt =  responseBody.jsonPath().getString("createdAt");
	          res_createdAt = res_createdAt.toString().substring(0, 11);
	          
	          //  3 : fetch the requestBody parameter

              JsonPath jsp_req = new JsonPath(requestBody);
        
        String req_name = jsp_req.getString("name");
        String req_job = jsp_req.getString("job");
        
        // 5 : Generate expected Date
        
        LocalDateTime currentdate = LocalDateTime.now();
        String expecteddate = currentdate.toString().substring(0, 11);
        
        // step 6: validation using TestNG Assertion
        
        Assert.assertEquals(res_name,req_name, " Name in ResponseBody is not equal to name sent in RequestBody");
        Assert.assertEquals(res_job, req_job,"Job in ResponseBody is not equal to job sent in RequestBody");
        Assert.assertNotNull(res_id,"id in ResponseBody is found to be null");
        Assert.assertEquals(res_createdAt, expecteddate,"createdAt in ResponseBody is not equal to Date generated");
        

	          
	          
	          
		    
	}

}
