package API_trigger_Reference;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;

public class post_API_trigger_Reference {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//step1 : Declare the needed variables
		        String hostname = "https://reqres.in";
				String resource = "/api/users";
				String headername= "content-type";
				String headervalue = "application/json";
				String requestBody = "{\r\n"
						+ "    \"name\": \"morpheus\",\r\n"
						+ "    \"job\": \"leader\"\r\n"
						+ "}";
				// step 2: configure the API and trigger
				RestAssured.baseURI=hostname;
				String responseBody = given().header(headername,headervalue).body(requestBody).when().post(resource).then().extract().response().asString();
          System.out.println(responseBody);
          
           // step 3 : parse the responseBody with json path and extract the response body parameter
          
          JsonPath jsp_res =new JsonPath(responseBody);
          
          String res_name = jsp_res.getString("name");
          String res_job = jsp_res.getString("job");
          String res_id = jsp_res.getString("id");
          String res_createdAt = jsp_res.getString("createdAt");
          res_createdAt = res_createdAt.toString().substring(0, 11);
          
          // step 4: parse the request body with json path and extract the resposnse body parameter
         
                JsonPath jsp_req = new JsonPath(requestBody);
          
          String req_name = jsp_req.getString("name");
          String req_job = jsp_req.getString("job");
          
          // 5 : Generate expected Date
          
          LocalDateTime currentdate = LocalDateTime.now();
          String expecteddate = currentdate.toString().substring(0, 11);
          
          // step 6: validation using TestNG Assertion
          
          Assert.assertEquals(res_name,req_name, " Name in ResponseBody is not equal to name sent in RequestBody");
          Assert.assertEquals(res_job, req_job,"Job in ResponseBody is not equal to job sent in RequestBody");
          Assert.assertNotNull(res_id,"id in ResponseBody is found to be null");
          Assert.assertEquals(res_createdAt, expecteddate,"createdAt in ResponseBody is not equal to Date generated");
          
	}

}
