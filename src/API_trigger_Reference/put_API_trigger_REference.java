package API_trigger_Reference;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;

import io.restassured.path.json.JsonPath;
import io.restassured.RestAssured;

public class put_API_trigger_REference {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//step 1 Declare the needed variables
		        String hostname ="https://reqres.in";
				String resource = "/api/users/2";
				String headername ="content-type";
				String headervalue ="application/json" ;
				String requestBody = "{\r\n"
						+ "    \"name\": \"morpheus\",\r\n"
						+ "    \"job\": \"zion resident\"\r\n"
						+ "}";
				//step 2 configure API and trigger
				RestAssured.baseURI= hostname;
				String responseBody = given().header(headername,headervalue).body(requestBody).when().put(resource).then().extract().response().asString();
		          System.out.println(responseBody);

	// step 3: parse the response body using json path extract the response body parameter
		          JsonPath jsp_res = new JsonPath(responseBody);
		          String res_name = jsp_res.getString("name");
		          String res_job = jsp_res.getString("job");
		          
		          String res_updatedAt = jsp_res.getString("updatedAt");
		          res_updatedAt = res_updatedAt.toString().substring(0, 11);
		          
		          
		          
		          //step 3 : parse the requestBody using json path extract the rquestBody parameter
		          JsonPath jsp_req = new JsonPath(requestBody);
		          String req_name = jsp_req.getString("name");
		          String req_job = jsp_req.getString("job");
		          
		          // step4 : Generate expected Date
		          LocalDateTime currentdate = LocalDateTime.now();
		          String expecteddate = currentdate.toString().substring(0, 11);
		          
		          // step 5 : validate using TestNG Assertion
		          Assert.assertEquals(res_name,req_name,"Name in ResponseBody is not equal to name sent in RequestBody");
		          Assert.assertEquals(res_job,req_job,"Job in ResposneBody is not equal to job sent in RequestBody");
		          Assert.assertEquals(res_updatedAt,expecteddate,"createdAt is in ResponseBody is not equal to createdAt in request");
		          
	}

}