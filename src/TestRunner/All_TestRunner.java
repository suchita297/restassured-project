package TestRunner;

import java.io.IOException;

import TestScript.Delete_TestScript;
import TestScript.Get_TestScript;
import TestScript.Patch_TestScript;
import TestScript.Post_Test_Script;
import TestScript.Put_TestScript;

public class All_TestRunner  {

	public static void main(String[] args) throws IOException {
		Post_Test_Script.execute();
		Put_TestScript.execute(); 
		Patch_TestScript.execute(); 
		 Get_TestScript.execute();  
		 Delete_TestScript.execute(); 
		 
	}

}
