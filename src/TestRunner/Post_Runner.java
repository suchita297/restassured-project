package TestRunner;

import java.io.IOException;

import TestScript.Post_Test_Script;
import TestScript.Post_param_Test_Script;
import commonmethods.Utilities;

public class Post_Runner {

	public static void main(String[] args) throws IOException {
		//Post_Test_Script.execute();
		Post_param_Test_Script.execute();
	}

}
