package TestScript;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import commonmethods.API_Trigger;
import commonmethods.Utilities;
import io.restassured.response.Response;

public class Delete_TestScript extends API_Trigger {
	
	public static void execute()  throws IOException{
		
		 File logfolder = Utilities.createFolder("Delete_API");
		 int statuscode=0;
		 for(int i=0;i<5;i++) {
		 Response response = Delete_API_Trigger(delete_request_body(),delete_endpoint());
		 // step 3 extract the status code
		 statuscode = response.statusCode();
		 System.out.println(statuscode);
		 if(statuscode==204) {
		 Utilities.createlogfile("Delete_API",logfolder,delete_endpoint(),delete_request_body(),response.getHeaders().toString(),response.getBody().asString());  
	        break;
	    }
	    else {
	    	System.out.println(" status code found in iteration:"+i+"is:"+ statuscode+",and is not equal expected status code hence retrying");
	    }

	    Assert.assertEquals(statuscode,204,"correct sttuscode is not found even after retrying for 5 time");
	    }
	    }
	

	}


