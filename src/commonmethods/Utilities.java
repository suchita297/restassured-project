package commonmethods;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Utilities {

	public static File createFolder(String foldername) {

		// step 1 fetch the current java project
		String projectFolder = System.getProperty("user.dir");
		System.out.println(projectFolder);

		// step 2 check if folder name coming in foldername is already exist in
		// projectFolder and create foldername accordingly

		File folder = new File(projectFolder + "\\Apilogs\\" + foldername);

		if (folder.exists()) {
			System.out.println(folder + ",  Already exists in java project:" + projectFolder);
		} else {
			System.out
					.println(folder + ",  Dosen't exists in java project:" + projectFolder + "," + "hence creating it");

			folder.mkdir();
			System.out.println(folder + ",  folder created in java project:" + projectFolder);
		}
		return folder;
	}

	public static void createlogfile(String Filename, File Filelocation, String endpoint, String requestBody,
			String responseHeader, String responseBody) throws IOException {

		// step 1 create and open text file
		File newtextFile = new File(Filelocation + "\\" + Filename + ".txt");
		System.out.println("File created with name:" + newtextFile.getName());

		// step 2 write data in the file
		FileWriter writedata = new FileWriter(newtextFile);
		writedata.write("Endpoint is : \n" + endpoint + "\n\n");
		writedata.write(" RequestBody is : \n" + requestBody + "\n\n");
		writedata.write("ResponseHeader is : \n" + responseHeader + "\n\n");
		writedata.write("ResponseBody is : \n" + responseBody);

		// step 3 save and close the file
		writedata.close();

	}

	public static ArrayList<String> ReadExcelData(String sheetname, String TestCase) throws IOException {
		ArrayList<String> arrayData =new ArrayList<String>();
		
		String projectdir = System.getProperty("user.dir");

		FileInputStream fs = new FileInputStream(projectdir + "\\Datafile\\Inputdata.xlsx");
		XSSFWorkbook wb = new XSSFWorkbook(fs);
		int countofsheet = wb.getNumberOfSheets();
		//System.out.println(countofsheet);

		for (int i = 0; i < countofsheet; i++) {
			if (wb.getSheetName(i).equals(sheetname)) {
				//System.out.println(wb.getSheetName(i));
				XSSFSheet sheet = wb.getSheetAt(i);
				Iterator<Row> rows = sheet.iterator();
				while (rows.hasNext()) {
					Row datarows = rows.next();
					String testcaseName = datarows.getCell(0).getStringCellValue();
					//System.out.println(testcaseName);
					if (testcaseName.equals(TestCase)) {
						Iterator<Cell> cellvalues = datarows.iterator();
						while (cellvalues.hasNext()) {
						String testdata = "";	
						
							Cell cell = cellvalues.next();
							CellType datatype = cell.getCellType();
							if(datatype.toString().equals("STRING")) {
								
						testdata = cell.getStringCellValue();
							}
							else if (datatype.toString().equals("NUMERIC")) {
								Double num_testdata =  cell.getNumericCellValue();
								testdata = String.valueOf(num_testdata);
							}
							
							
							System.out.println(testdata);
							arrayData.add(testdata);
							
						}
						break;
					}
				}
				break;
				} else {
				System.out.println("no sheet found of name :" + sheetname + "in current iteration" + i);
			}
		}
		wb.close();
		return arrayData;

	}
	}

